# API

## Feature Type
```@docs
Features
```

## Feature Extractors
```@docs
FeatureExtractor
Frames
OverlapAdd
FFT
IFFT
MelFilterBank
InvMelFilterBank
DCT
IDCT
Autocorrelation
AddDeltas
```

## Utils

```@docs
SpeechFeatures.griffim_lin
SpeechFeatures.filterbank
SpeechFeatures.invfilterbank
```

## Index

```@index
```
