# SpeechFeatures.jl

A Julia pacakge to extract acoustic representation for speech technologies:
Framing, Fourier Transform, MFCC, Autocorrelation, etc.


## Contents

```@contents
Pages = ["index.md", "installation.md"]
```

## License
This software is provided under the [CeCILL-B license](https://cecill.info/licences.en.html)

## Authors

- Lucas Ondel Yang
- Martin Karafiat
- Martin Kocour
- Nicolas Denier

![](https://ptal.lisn.upsaclay.fr/assets/lisn-ups-cnrs.png)
