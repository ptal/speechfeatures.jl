push!(LOAD_PATH,"..")

using Documenter, SpeechFeatures
using Documenter.Remotes
using AudioSources

include("deployconfig.jl")

makedocs(
    sitename="SpeechFeatures",
    repo = Remotes.GitLab("gitlab.lisn.upsaclay.fr", "PTAL", "AcousticProcessing/SpeechFeatures.jl"),
    doctest = false,
    modules = [SpeechFeatures],
    pages = [
        "Home" => "index.md",
        "Installation" => "installation.md",
        #"Examples" => "examples.md",
        #"API" => "api.md"
    ],
    format = Documenter.HTMLWriter.HTML(example_size_threshold = 0)
)

config = GitLabHTTPS()

deploydocs(
    repo = "gitlab.lisn.upsaclay.fr/PTAL/AcousticProcessing/SpeechFeatures.jl",
    branch = "docs",
    deploy_config = GitLabHTTPS(),
    versions=["latest"=>"v^", "v#.#.#"]
)
