# SPDX-License-Identifier: CECILL-C

module SpeechFeatures

using FFTW
using Statistics: mean
using AudioSources
import LinearAlgebra: norm


# Features type
export Features, data, originsrate, srate

# Feature extractors
export FeatureExtractor,
       AddDeltas,
       DCT,
       IDCT,
       Frames,
       MelFilterBank,
       InvMelFilterBank,
       OverlapAdd,
       FFT,
       IFFT,
       Autocorrelation


include("features.jl")
include("utils.jl")
include("extractors.jl")


# Not Ready yet
# export SpecAugment
#include("specaugment.jl")

end
