# SPDX-License-Identifier: CECILL-C


"""
    FeatureExtractor

Abstract type of feature extractors.
"""
abstract type FeatureExtractor end


function _getfieldstr(instance, name)
    f = getfield(instance, name)
    if f isa AbstractString
        "\"" * f * "\""
    else
        string(f)
    end
end

function Base.show(io::IO, extractor::FeatureExtractor)
    print(io, typeof(extractor), "(")
    print(io, join(["$(name)=$(_getfieldstr(extractor, name))" for name in fieldnames(typeof(extractor))], ", "))
    print(io, ")")
end


"""
    Frames(; kwargs...)

Initialize frames extractor with default values if not specified.
# Arguments
- `frameduration = 0.025` in seconds
- `framestep = 0.01` time between two frames in seconds
- `dithering = 0.0` add gaussian noise to the signal
- `preemph = 0.97` improve signal-to-noise ratio by boosting high frequencies
- `removedc = true`
- `window = "hann"` framing window, one of ["hann", "hamming", "povey", "rectangular"]
- `dropedge = true`
"""
struct Frames <: FeatureExtractor
    frameduration::Float64 # s
    framestep::Float64 # s
    dithering::Float64
    preemph::Float64
    removedc::Bool
    padding::Int
    window::String
    dropedge::Bool
end

function Frames(; frameduration=0.025, framestep=0.01, dithering=0.0, preemph=0.97, removedc=true, padding=0, window="hann", dropedge=true)
    Frames(frameduration, framestep, dithering, preemph, removedc, padding, window, dropedge)
end


function (f::Frames)(_x::AbstractVector, fs)
    x = copy(_x)

    # Properties
    framesize = ceil(Int, f.frameduration*fs)
    framestep = ceil(Int, f.framestep*fs)

    haskey(windowTypes, f.window) || throw(ArgumentError("unkown window type $(f.window)"))

    # Copy the window, it allocates a buffer but save many computations.
    window = copy(windowTypes[f.window](Float64, framesize))

    # DC removal
    if f.removedc
        dc = sum(x) / length(x)
        x = map(a -> a - dc, x)
    end

    # PREEMPH
    iszero(f.preemph) || preemphasis!(x, f.preemph)

    # DITHERING
    iszero(f.dithering) || map!(a -> a + f.dithering * randn(), x, x)

    data = copy(FrameMatrix(x, framesize, framestep, window, f.padding, f.dropedge))

    new_srate = fs/framestep
    Features(data, new_srate, fs)
end


"""
    OverlapAdd(; hopsize = 160)

Create a signal from frames using the [overlap-add method](https://en.wikipedia.org/wiki/Overlap%E2%80%93add_method).
`hopsize` is the "shift" in samples between two frames. The default value `160`
corresponds to a 10 milliseconds shift at 16kHz.
"""
struct OverlapAdd <: FeatureExtractor
    hopsize::Int
end

function OverlapAdd(; hopsize=160)
    OverlapAdd(hopsize)
end


function (f::OverlapAdd)(X)
    L = (size(X, 2) - 1) * f.hopsize + size(X, 1) + 1
    y = zeros(eltype(X), L)

    offset = 1
    for n in 1:size(X, 2)
        for i in 1:size(X, 1)
            y[offset+i] += X[i,n]
        end
        offset += f.hopsize
    end

    Features(y, X.originsrate, X.originsrate)
end


"""
    FFT()

Apply the Fast Fourier Transform per-frame (see [`Frames`](@ref)).
"""
struct FFT <: FeatureExtractor end

function (f::FFT)(X)
    # Make sure that the number of rows in `X` is a power of 2.
    pad = nextpow(2, size(X, 1)) - size(X, 1)
    _X = vcat(X.data, zeros(eltype(X), pad, size(X, 2)))

    #window_padding = nextpow(2, framesize) - framesize
    P = plan_rfft(_X, 1)
    Y = P * _X
    Features(Y, X.srate, X.originsrate, size(Y, 1))
end

"""
    IIFFT()

Apply the inverse Fast Fourier Transform per-frame (see [`Frames`](@ref)).
"""
struct IFFT <: FeatureExtractor end

function (f::IFFT)(X)
    #window_padding = nextpow(2, framesize) - framesize
    P = plan_irfft(X.data, 2*(X.numfreqbins-1), 1)
    Y = P * X
    Features(Y, X.srate, X.originsrate)
end


"""
    MelFilterBank(; lofreq=80, hifreq=-400, overlap=true, smooth=false)

Create mel-scale filter bank. `numfilters` is the number of filters,
`lofreq` is the low cut-off frequency and  `hifreq` is
the high cut-off frequency (relative to the Nyquist frequency). If `overlap = false`
the filters are disjoint and if `smooth = true` the filters are bell-shaped
rather than triangular.
"""
struct MelFilterBank <: FeatureExtractor
    numfilters::Int
    lofreq::Int
    hifreq::Int
    overlap::Bool
    smooth::Bool
end

MelFilterBank(; numfilters=26, lofreq=80, hifreq=-400, overlap=true, smooth=false) =
    MelFilterBank(numfilters, lofreq, hifreq, overlap, smooth)

function (f::MelFilterBank)(X)
    # We assume the window length is even
    freqbins = rfftfreq(2*X.numfreqbins-1, X.originsrate)
    F = filterbank(f.numfilters, freqbins, lofreq=f.lofreq, hifreq=f.hifreq, overlap = f.overlap, smooth = f.overlap)
    Features(F * X.data, X.srate, X.originsrate, X.numfreqbins, f.numfilters)
end


"""
    InvMelFilterBank(; lofreq=80, hifreq=-400, overlap=true, smooth=false)

Create the "inverse" mel-scale filter bank. See [`MelFilterBank`](@ref) for the
parameters.
"""
struct InvMelFilterBank <: FeatureExtractor
    lofreq::Int
    hifreq::Int
    overlap::Bool
    smooth::Bool
end

InvMelFilterBank(; lofreq=80, hifreq=-400, overlap=true, smooth=false) = InvMelFilterBank(lofreq, hifreq, overlap, smooth)

function (f::InvMelFilterBank)(X)
    freqbins = rfftfreq(2*X.numfreqbins-1, X.originsrate)
    F = filterbank(X.nummelbins, freqbins, lofreq=f.lofreq, hifreq=f.hifreq)
    Features(F' * X.data, X.srate, X.originsrate, X.numfreqbins)
end


"""
    DCT(nbases=13)

Extractor of the `nbases` first dimension of the Discrete Cosine transform.
"""
struct DCT <: FeatureExtractor
    nbases::Int
end

DCT(; nbases=13) = DCT(nbases)

function (f::DCT)(X::Features)
    melcepstrum = dct(X.data, 1)[1:f.nbases,:]
    Features(melcepstrum, X.srate, X.originsrate, X.numfreqbins, X.nummelbins, f.nbases)
end


"""
    IDCT()

Extractor of the `nbases` first dimension of the Discrete Cosine transform.
"""
struct IDCT <: FeatureExtractor end

function (f::IDCT)(X::Features)
    padding = X.nummelbins - X.ndctbases
    pX = vcat(X.data, zeros(eltype(X), padding, size(X, 2)))
    Features(idct(pX, 1), X.srate, X.originsrate, X.numfreqbins, X.nummelbins)
end


"""
    Autocorrelation()

Create a autocorrelation extractor.
"""
struct Autocorrelation <: FeatureExtractor end

function (f::Autocorrelation)(X::Features)
    data = irfft(abs.(rfft(X, 1)).^2, size(X, 1), 1)
    Features(data, X.srate, X.originsrate)
end


"""
    AddDeltas(; kwargs...)

Create delta features extractor.

# Arguments
- `order = 2` derivative order
- `winlen = 2` length of delta window
"""
struct AddDeltas <: FeatureExtractor
    order::Int
    winlen::Int
end


AddDeltas(; order=2, winlen=2) = AddDeltas(order,winlen)

function (f::AddDeltas)(X::Features)
    X_and_deltas = [X.data]
    for _ in 1:f.order
        push!(X_and_deltas, delta(X_and_deltas[end], f.winlen))
    end
    data = vcat(X_and_deltas...)
    Features(data, X.srate, X.originsrate, X.numfreqbins, X.nummelbins, X.ndctbases)
end

