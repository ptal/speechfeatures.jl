# SPDX-License-Identifier: CECILL-C

#======================================================================
Framing
======================================================================#

struct FrameMatrix{T<:AbstractFloat,W<:AbstractVector{T}} <: AbstractArray{T,2}
    x::Vector{T}
    framesize::Int64
    stride::Int64
    window::W
    padding::Int
    dropedge::Bool
end

function get_nb_columns(f::FrameMatrix)
    if f.dropedge
        Int(ceil((length(f.x) - f.framesize + 1) / f.stride))
    else
        Int(ceil(length(f.x) / f.stride))
    end
end

function Base.size(f::FrameMatrix)
    f.framesize + f.padding, get_nb_columns(f)
end

function Base.getindex(f::FrameMatrix{T}, i::Integer, j::Integer) where T
    # index in the original signal
    n = (j - 1) * f.stride + i
    n ≤ length(f.x) ? (i ≤ f.framesize ? f.window[i] * f.x[n] : T(0) ) : T(0)
end

#======================================================================
Liftering
======================================================================#

function makelifter(N, L)
	t = Vector(1:N)
	1 .+ L/2 * sin.(π * t / L)
end

#======================================================================
Pre-emphasis
======================================================================#

function preemphasis!(x, k)
	prev = x[1]
	for i in 2:length(x)
        prev2 = x[i]
		x[i] = x[i] - k*prev
		prev = prev2
	end
end

#======================================================================
Delta features
======================================================================#

function delta(X::AbstractMatrix{T}, deltawin::Int = 2) where T
    D, N = size(X)
    Δ = zeros(T, D, N)
    norm = T(2 * sum(collect(1:deltawin).^2))
    for n in 1:N
        for θ in 1:deltawin
            Δ[:, n] += (θ * (X[:,min(N, n+θ)] - X[:,max(1,n-θ)])) / norm
        end
    end
    Δ
end

#======================================================================
Window functions
======================================================================#

struct CosineWindow{T} <: AbstractVector{T}
    size::Int
    a₀::T
    p::T
end

Base.size(w::CosineWindow) = (w.size,)
Base.getindex(w::CosineWindow, n) = (w.a₀ - (1 - w.a₀) * cos(2π * (n - 1) / w.size))^w.p

HannWindow(T, N) = CosineWindow{T}(N, 1/2, 1)
PoveyWindow(T, N) = CosineWindow{T}(N, 1/2, 0.85)
HammingWindow(T, N) = CosineWindow{T}(N, 25/46, 1)

struct RectangularWindow{T} <: AbstractVector{T}
    size::Int
end

RectangularWindow(T, N) = RectangularWindow{T}(N)
Base.size(w::RectangularWindow) = (w.size,)
Base.getindex(w::RectangularWindow, n) = 1

const windowTypes=Dict(
    "hann" => HannWindow,
    "hamming" => HammingWindow,
    "povey" => PoveyWindow,
    "rectangular" => RectangularWindow
)

#======================================================================
Filter bank
======================================================================#

mel2freq(mel::Real) = 700 * (exp(mel / 1127) - 1)
freq2mel(freq::Real) = 1127 * log(1 + (freq / 700))

"""
    filterbank(n, freqs; lofreq = 80, hifreq = -400)

Create a bank of filters that redistributes the spectral energy into
linearly space mel bins.
"""
function filterbank(n, freqbins; lofreq = 80, hifreq = -400, overlap=true, smooth=false)
    # Cut-off frequencies
    low = lofreq
    high = freqbins[end] + hifreq

    # Mel bin corresponding to the triangles' peak.
    Δm = (freq2mel(high) - freq2mel(low)) / (n + 1)
    melbins = freq2mel(lofreq) .+ collect((1:n) .* Δm)

    Δ = overlap ? Δm : Δm/2

    kernel(m1, m2) = smooth ? max(0, 1 - ((m1 - m2) / Δ)^2) : max(0, 1 - abs((m1 - m2) / Δ))

    F = zeros(n, length(freqbins))
    for (i, mb) in enumerate(melbins)
        for (j, fb) in enumerate(freqbins)
            F[i,j] = kernel(freq2mel(fb), mb)
        end
    end
    F
end


"""
    invfilterbank(n, freqs; lofreq = 80, hifreq = -400)

Create the "inverse" of mel-space filter bank. See [`filterbank`](@ref)
"""
function invfilterbank(n, freqbins; lofreq = 80, hifreq = -400)
    # Cut-off frequencies
    low = lofreq
    high = freqbins[end] + hifreq

    Δf = freqbins[end] / (length(freqbins) - 1)

    # Mel bin corresponding to the triangles' peak.
    Δm = (freq2mel(high) - freq2mel(low)) / (n + 1)
    melbins = freq2mel(lofreq) .+ collect((1:n) .* Δm)

    F = zeros(length(freqbins), n)
    for (i, fb) in enumerate(freqbins)
        for (j, mb) in enumerate(melbins)
            F[i,j] = max(0, 1 - abs((mel2freq(mb) - fb) / (Δf)))
        end
    end
    F
end


"""
    griffim_lin(magX, STFT, Fs; threshold=1e-3, maxiter=10000)

Return a value spectrogram given with magnitude spectrum `magX` and phase
obtained from applying the Griffim-Lin algorithm until the convergence
`threshold` or `maxiter` is reached. `STFT` is a transform that takes a signal
and a frequency sampling (`fs`) as arguments and return the complex Short Term
Spectrum of the same dimension as `magX`.
"""
function griffim_lin(magX::Features, STFT, fs; threshold=1e-3, maxiter=10000)
    ϕ  = zeros(eltype(magX), size(magX)...)

    metadata = (magX.srate, magX.originsrate, magX.numfreqbins, magX.nummelbins, magX.ndctbases)

    prevL = Inf
    P = OverlapAdd() ∘ IFFT()
    for it in 1:maxiter
        X = Features(magX .* cis.(ϕ), metadata...)
        newX = STFT(P(X), fs)
        newX = Features(newX.data[:, 1:size(X, 2)], metadata...)

        L = norm(newX .- X)
        ϕ = angle.(newX)

        abs(prevL - L) < threshold && break
        prevL = L
    end
    Features(magX .* cis.(ϕ), magX.srate, magX.originsrate, magX.numfreqbins)
end


#======================================================================
Display
======================================================================#

function html_element(s::String, tag::String, options::String="")
    "<$tag $options>$s</$tag>"
end

function html_join(row::AbstractArray, tag::String)
    join(html_element.(row, tag))
end

function html_table(table::AbstractMatrix{String})
    html_element(
        html_join(
            html_join.(eachrow(table), "td")
        , "tr")
    , "table")
end

function html_table(table::AbstractMatrix{String}, header::AbstractVector{String})
    html_element(
        html_element(html_join(header, "th"), "tr")*
        html_join(
            html_join.(eachrow(table), "td")
        , "tr")
    , "table")
end

function html_table(table::AbstractMatrix{String}, header::AbstractVector{String}, title::String)
    html_element(
        html_element(html_element("$title", "th", "colspan=$(length(header))"), "tr")*
        html_element(html_join(header, "th"), "tr")*
        html_join(
            html_join.(eachrow(table), "td")
        , "tr")
    , "table")
end

