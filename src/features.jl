# SPDX-License-Identifier: CECILL-B


"""
    Features{T,N} <: AbstractArray{T,N}

Array-type equipped with a sample rate parameters associated to the last
dimension.
"""
struct Features{T,N} <: AbstractArray{T,N}
    data::Array{T,N}
    srate::Float64
    originsrate::Int
    numfreqbins::Union{Missing,Int}
    nummelbins::Union{Missing,Int}
    ndctbases::Union{Missing,Int}
end

Features(data, srate, srate_origin, numfreqbins=missing, nummelbins=missing, ndctbases=missing) =
    Features{eltype(data),ndims(data)}(data, srate, srate_origin, numfreqbins, nummelbins, ndctbases)

Base.size(x::Features) = size(x.data)
Base.getindex(x::Features, inds...) = x.data[inds...]
Base.setindex!(x::Features, v, inds...) = x.data[inds...] = v

Base.BroadcastStyle(::Type{<:Features}) = Broadcast.ArrayStyle{Features}()

_find_fea_array(bc::Base.Broadcast.Broadcasted) = _find_fea_array(bc.args)
_find_fea_array(args::Tuple) = _find_fea_array(_find_fea_array(args[1]), Base.tail(args))
_find_fea_array(x) = x
_find_fea_array(::Tuple{}) = nothing
_find_fea_array(x::Features, rest) = x
_find_fea_array(::Any, rest) = _find_fea_array(rest)

function Base.similar(bc::Broadcast.Broadcasted{Broadcast.ArrayStyle{Features}}, ::Type{ElType}) where ElType
    x = _find_fea_array(bc)
    Features(similar(x.data, ElType), x.srate, x.originsrate, x.numfreqbins, x.nummelbins, x.ndctbases)
end

