# SpeechFeatures.jl

SpeechFeatures.jl is a Julia package for extracting acoustic features for
speech processing.


## Installation

This package is part of the PTAL tool collection and requires the
[PTAL registry](https://gitlab.lisn.upsaclay.fr/ptal/registry) to be installed.

Once the registry has been added, SpeechFeatures can be installed with the
Julia package manager by typing in Pkg REPL mode
```
pkg> add SpeechFeatures
```

## Usage

```julia
using AudioSources
using SpeechFeatures

example = URLAudioSource("https://catalog.ldc.upenn.edu/desc/addenda/LDC93S1.wav")

# chain any feature you want
features = MFCC() ∘ FBANK() ∘ STFT() ∘ Frames()
X, props = features(example)

using Plots
heatmap(log.(abs.(X)))
```

## License

This software is provided under the [CeCILL-B license](https://cecill.info/licences.en.html)
(see [`/license`](/license))

