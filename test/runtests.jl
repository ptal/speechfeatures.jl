
#==============================================================================

IMPORTANT

These tests ensure that the feature extraction tools are
"functional", i.e. they return consistent data structures but they do not check
whether the values are correct.

==============================================================================#


using AudioSources
using NPZ
using SpeechFeatures
using Test
using Statistics

@testset "Window functions" begin
    N = 10
    T = Float32

    w = SpeechFeatures.RectangularWindow(T, N)
    ref = ones(T, N)
    @test all(w .≈ ref)
    @test eltype(w) == T

    w = SpeechFeatures.HannWindow(T, N)
    ref = 0.5 .* (1 .- cos.(2π .* Vector(0:N-1) ./ N ))
    @test all(w .≈ ref)
    @test eltype(w) == T

    w = SpeechFeatures.HammingWindow(T, N)
    ref = (T(25/46)) .- (1 - T(25/46)) .* cos.(T(2π) .* Vector(0:N-1) ./ N )
    @test all(w .≈ ref)
    @test eltype(w) == T
end

@testset "Features" begin
    source = RawAudioSource(randn(16000, 1), 16000)
    x, fs = load(source, channels = 1)

    X = Frames(dropedge = false)(x, fs)
    @test (X .+ 1) isa Features

    X = Frames(dropedge = false)(x, fs)
    @test size(X) == (400, 100)
    @test size(X.data) == size(X)
    @test X.originsrate == 16000
    @test X.srate == 100
    @test X.numfreqbins === missing
    @test X.nummelbins === missing
    @test X.ndctbases === missing

    y = OverlapAdd()(X)
    @test size(y, 1) ≥ 16000
    @test y.originsrate == 16000
    @test y.srate == 16000
    @test y.numfreqbins === missing
    @test y.nummelbins === missing
    @test y.ndctbases === missing

    X = FFT()(X)
    @test size(X) == (257, 100)
    @test X.originsrate == 16000
    @test X.srate == 100
    @test X.numfreqbins == 257
    @test X.nummelbins === missing
    @test X.ndctbases === missing

    X̂ = IFFT()(X)
    @test size(X̂) == (512, 100)
    @test X̂.originsrate == 16000
    @test X̂.srate == 100
    @test X̂.numfreqbins === missing
    @test X̂.nummelbins === missing
    @test X̂.ndctbases === missing

    X = MelFilterBank()(X)
    @test size(X) == (26, 100)
    @test X.originsrate == 16000
    @test X.srate == 100
    @test X.numfreqbins == 257
    @test X.nummelbins == 26
    @test X.ndctbases === missing

    X̂ = InvMelFilterBank()(X)
    @test size(X̂) == (257, 100)
    @test X̂.originsrate == 16000
    @test X̂.srate == 100
    @test X̂.numfreqbins == 257
    @test X̂.nummelbins === missing
    @test X̂.ndctbases === missing

    X = DCT()(X)
    nbases = 13
    @test size(X) == (nbases, 100)
    @test X.originsrate == 16000
    @test X.srate == 100
    @test X.numfreqbins == 257
    @test X.nummelbins == 26
    @test X.ndctbases == 13

    X̂ = IDCT()(X)
    @test size(X̂) == (26, 100)
    @test X̂.originsrate == 16000
    @test X̂.srate == 100
    @test X̂.numfreqbins == 257
    @test X̂.nummelbins == 26
    @test X̂.ndctbases === missing

    X = AddDeltas()(X)
    @test size(X) == (nbases*3, 100)
    @test X.originsrate == 16000
    @test X.srate == 100
    @test X.numfreqbins == 257
    @test X.nummelbins == 26
    @test X.ndctbases == 13
end


@testset "GriffimLin" begin
    magX = Features(exp.(randn(257, 300)), 100, 16000, 257)
    STFT = FFT() ∘ Frames(dropedge=false)
    X = SpeechFeatures.griffim_lin(magX, STFT, 16000; maxiter = 1)
    @test eltype(X) <: Complex
    @test size(X) == (257, 300)
end


@testset "Autocorrelation" begin
    frame = npzread("frame.npy")
    ref_autocorr = npzread("autocorr.npy")

    extractor = Autocorrelation() ∘ Frames(window = "rectangular", removedc = false, preemph=0, padding = 399)
    hyp_autocorr = extractor(frame, 16000)
    @test hyp_autocorr[1:400] ≈ ref_autocorr
end

#=
@testset "SpecAugment" begin
    source = URLAudioSource("https://catalog.ldc.upenn.edu/desc/addenda/LDC93S1.wav")
    signal, fs = loadaudio(source)

    # Create fbank matrix
    fbank_config = Dict("numfilters" => 80)
    fbank_fce = FBANK(fs, fbank_config)
    fbank     = fbank_fce(signal[:, 1], fs)
    fbank_orig = copy(fbank)

    # Check time-warping

    # Test time warping only
    dct_conf = Dict(
        "apply_time_warp"  => true,
	"apply_freq_mask"  => false,
        "apply_time_mask"  => false,
        )
    SA_obj = SpecAugment(dct_conf)
    fbank_sa = SA_obj(fbank)
    @test size(fbank_sa) == size(fbank_orig)   # Size should be same
    @test fbank_sa       != fbank_orig         # fbank not

    # Freq
    dct_conf = Dict(
        "apply_time_warp"  => false,
	"apply_freq_mask"  => true,
        "apply_time_mask"  => false,
        )
    SA_obj = SpecAugment(dct_conf)
    fbank_sa = SA_obj(fbank)

    max_zeroed_freqs = SA_obj.params["freq_mask_width_range"][2] * SA_obj.params["num_freq_mask"]
    zeroed_freqs     = sum(mean(fbank_sa, dims=2) .== 0)
    max_zeroed_times = SA_obj.params["time_mask_width_range"][2] * SA_obj.params["num_time_mask"]
    zeroed_times     = sum(mean(fbank_sa, dims=1) .== 0)
    @test 0 < zeroed_freqs <= max_zeroed_freqs
    @test zeroed_times == 0
    @test fbank_sa    != fbank_orig

    # Time
    dct_conf = Dict(
        "apply_time_warp"  => false,
	"apply_freq_mask"  => false,
        "apply_time_mask"  => true,
        )
    SA_obj = SpecAugment(dct_conf)
    fbank_sa = SA_obj(fbank)

    max_zeroed_freqs = SA_obj.params["freq_mask_width_range"][2] * SA_obj.params["num_freq_mask"]
    zeroed_freqs     = sum(mean(fbank_sa, dims=2) .== 0)
    max_zeroed_times = SA_obj.params["time_mask_width_range"][2] * SA_obj.params["num_time_mask"]
    zeroed_times     = sum(mean(fbank_sa, dims=1) .== 0)
    @test zeroed_freqs == 0
    @test 0 < zeroed_times <= max_zeroed_times
end
=#
